package me.imsanti.dev.blockregen;

import me.imsanti.dev.blockregen.listeners.BreakListener;
import me.imsanti.dev.blockregen.managers.BlockManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class BlockRegen extends JavaPlugin {

    private final BlockManager blockManager = new BlockManager();

    @Override
    public void onEnable() {
        registerEvents();
        // Plugin startup logic
        blockManager.loadDimensions();
    }

    @Override
    public void onDisable() {
        //Plugin shutdown logic
    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new BreakListener(this), this);
    }

    public BlockManager getBlockManager() {
        return blockManager;
    }
}
