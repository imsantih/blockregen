package me.imsanti.dev.blockregen.listeners;

import me.imsanti.dev.blockregen.BlockRegen;
import me.imsanti.dev.blockregen.objects.Dimension;
import net.kyori.adventure.sound.Sound;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class BreakListener implements Listener {

    private final BlockRegen blockRegen;

    public BreakListener(final BlockRegen blockRegen) {
        this.blockRegen = blockRegen;
    }

    @EventHandler
    private void handleBreak(final BlockBreakEvent event) {
        if(event.isCancelled()) return;
        final Material blockType = event.getBlock().getType();
        if(!blockRegen.getBlockManager().getLoadedDimensions().containsKey(event.getBlock().getWorld().getName())) return;

        final Dimension dimension = blockRegen.getBlockManager().getLoadedDimensions().get(event.getBlock().getWorld().getName());
            if(dimension.isBlockAllowed(event.getBlock().getType())) {
                event.setCancelled(true);
                event.getPlayer().getInventory().addItem(new ItemStack(Material.valueOf(event.getBlock().getType().name())));
                event.getBlock().setType(Material.BEDROCK);

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        event.getBlock().setType(blockType);
                    }
                }.runTaskLater(blockRegen, dimension.getRegenTime() * 20);
            }else {
                event.setCancelled(true);
        }
    }
}
