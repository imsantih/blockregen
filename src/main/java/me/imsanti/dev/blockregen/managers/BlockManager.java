package me.imsanti.dev.blockregen.managers;

import me.imsanti.dev.blockregen.objects.Dimension;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BlockManager {

    private final Map<String, Dimension> loadedDimensions = new HashMap<>();
    private final List<Material> endBlocks = new ArrayList<>();


    private void fillList() {
        endBlocks.add(Material.END_STONE);
        endBlocks.add(Material.BLACKSTONE);
        endBlocks.add(Material.OBSIDIAN);
        endBlocks.add(Material.SHROOMLIGHT);
    }

    public void loadDimensions() {
        fillList();
        loadedDimensions.put("MundoRPG_the_end", new Dimension("MundoRPG_the_end", endBlocks, 30));
        loadedDimensions.put("MundoRPG_Nether", new Dimension("MundoRPG_the_end", endBlocks, 30));

    }

    public Map<String, Dimension> getLoadedDimensions() {
        return loadedDimensions;
    }
}
