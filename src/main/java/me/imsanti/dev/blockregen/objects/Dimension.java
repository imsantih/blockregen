package me.imsanti.dev.blockregen.objects;

import org.bukkit.Material;

import java.util.List;

public class Dimension {

    private final String worldName;
    private final List<Material> allowedBlocks;
    private final int regenTime;

    public Dimension(final String worldName, final List<Material> allowedBlocks, final  int regenTime) {
        this.worldName = worldName;
        this.allowedBlocks = allowedBlocks;
        this.regenTime = regenTime;
    }

    public List<Material> getAllowedBlocks() {
        return allowedBlocks;
    }

    public boolean isBlockAllowed(final Material material) {
        return allowedBlocks.contains(material);
    }

    public String getWorldName() {
        return worldName;
    }

    public int getRegenTime() {
        return regenTime;
    }
}
